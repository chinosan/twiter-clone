import React, { ReactNode, FC, createContext, useContext, useState, useEffect } from "react";
import { onAuthStateChanged, getAuth } from "firebase/auth";
import fb_app from "./config";

interface Props {
    children?: ReactNode
}

interface UserData {
  displayName: string;
  email: string;
  photoURL: string;
  uid: string;
}

const auth = getAuth(fb_app);

export const AuthContext = createContext({});

export const useAuthContext = () => useContext(AuthContext);

export const AuthContextProvider: FC<Props> = ({ children }) => {
  const [user, setUser] = useState<UserData | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      let currentUser = null;
      if (user) {

        const {displayName, email, photoURL, uid} = user

        const userpartial = {
          displayName, email, photoURL, uid
        } as UserData;

        setUser(userpartial);
        currentUser = userpartial

      } else {
        setUser(null);
      }
      setLoading(false);

      // console.log("usaurio actual: ")
      // console.log(currentUser)
      // console.log(userD)
      // console.log(loading)
    });

    return () => unsubscribe();
  }, []);

  return (
    <AuthContext.Provider value={ {user} }>
      {loading ? <div>Loading...</div> : children}
    </AuthContext.Provider>
  );
};
