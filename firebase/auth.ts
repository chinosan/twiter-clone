import fb_app from "./config";
import {
  EmailAuthProvider,
  getAuth,
  GoogleAuthProvider,
  signInWithEmailAndPassword,
  signInWithPopup,
  signOut,
} from "firebase/auth";
import {
  getFirestore,
  doc,
  setDoc,
  getDoc,
  Timestamp,
  updateDoc,
  query,
  collection,
  getDocs,
  where,
  or,
} from "firebase/firestore";
import type { User as FirebaseUser } from "firebase/auth";
import { User } from "../models/user";

const db = getFirestore(fb_app);

const auth = getAuth(fb_app);
// console.log(auth);

export async function signInWithGoogle() {
  let res: any = null;
  let error: any = null;

  try {
    const provider = new GoogleAuthProvider();
    const result = await signInWithPopup(auth, provider);

    const credential = GoogleAuthProvider.credentialFromResult(result);
    const token = credential?.accessToken;
    const user: any = result.user;
    await updateUser(user);
    await createUserName(user);
    const route = "/";
    location = route as any;
  } catch (error: any) {
    const response = handleAuthError(error.code);
  }
}

export async function loginWithEmail(email: string, password: string) {
  try {
    const result = await signInWithEmailAndPassword(auth, email, password);

    const credential = EmailAuthProvider.credential(email, password);
    const token = credential.providerId;
    const user = result.user;
    await updateUser(user);
    const route = "/";
    location = route as any;
  } catch (error: any) {
    const errorCode = error.code;
    // const errorMessage = error.message;
    const response = handleAuthError(errorCode);
    // console.log(response);
    return response;
  }
}

const updateUser = async (user: FirebaseUser) => {
  try {
    const data = await getUserData(user);
    const ref = doc(db, "users", user.uid);
    await setDoc(ref, data, { merge: true });
  } catch (error: any) {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log(errorMessage);
  }
};

export async function getUserData(user: FirebaseUser): Promise<User> {
  const { creationTime } = user.metadata;
  console.log(creationTime);
  const data: User = {
    uid: user.uid,
    email: user.email,
    emailVerified: user.emailVerified,
    displayName: user.displayName,
    createdAt: Timestamp.fromDate(new Date(creationTime ? creationTime : 0)),
  } as User;

  return data;
}

export const createUserName = async (user: User) => {
  if (
    user.userName == "" ||
    user.userName == null ||
    user.userName == undefined
  ) {
    try {
      const genericInitials = "G2-U";
      const complement = Math.random().toString(36).substring(2, 8);
      const newUserName = genericInitials + complement;
      const ref = doc(db, "users", user.uid);
      await updateDoc(ref, { userName: newUserName });
    } catch (error) {
      console.log("error: " + error);
    }
  } else {
    console.log("Ya cuenta con nombre de usuario");
  }
};

export async function getUser(user: string) {
  let userresponse = null;
  const q = query(
    collection(db, "users"),
    or(where("email", "==", user), where("userName", "==", user))
  );

  const querySnapshot = await getDocs(q);
  querySnapshot.forEach((doc) => {
    userresponse = doc.data();
  });

  return userresponse;
}

export async function logOut() {
  try {
    await signOut(auth);
    location = "/" as any;
  } catch (error: any) {
    const errorCode = error.code;
    const errorMessage = error.message;
    const response = handleAuthError(error.code);
    console.log(response);
  }
}

export function handleAuthError(errorCode: string) {
  let error: string = "";
  switch (errorCode) {
    case "auth/email-already-in-use":
      error = "El correo ya ha sido utilizado.";
      break;
    case "auth/user-not-found":
      error = "El usuario NO existe.";
      break;
    case "auth/wrong-password":
      error = "Contraseña Incorrrecta.";
      break;
    case "auth/weak-password":
      error = "La contraseña deberia ser más grande";
      break;
    case "auth/invalid-email":
      error = "Correo NO válido.";
      break;
    default:
      error = "Error de autenticación, Intentelo de nuevo.";
      break;
  }
  return error;
}
