import { createSlice } from "@reduxjs/toolkit";
// import { useAuthContext } from "../../firebase/AuthContext";

const usersSlice = createSlice({
  name: "user",
  initialState: {
    hasUser: false,
    dataUser: { displayName: "", email: "", uid: "", photoURL: "" },
  },
  reducers: {
    setCurrentUserlogged(state, action) {
      // console.log(action.payload);

      if (action.payload === null) {
        state.hasUser = false;
        state.dataUser = {
          displayName: "",
          email: "",
          uid: "",
          photoURL: "",
        };
        return;
      }

      const { displayName, email, photoURL, uid } = action.payload;
      state.hasUser = true;
      state.dataUser = {
        displayName,
        email,
        uid,
        photoURL,
      };
    },
  },
});

export const userAction = usersSlice.actions;

export default usersSlice.reducer;
