import React from "react";
import Image from "next/image";
import Divider from "../shared/divider";
import {loginWithEmail, signInWithGoogle, getUser} from "../../firebase/auth";
import { FaTwitter } from "react-icons/fa";


interface ModalProps{
    openModal: boolean;
    onClose: ()=> void;
    isregisterT:boolean;
    setIsregisterT:any
}

const Modal = ({openModal, onClose, isregisterT, setIsregisterT}:ModalProps) => {

    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [step, setStep] = React.useState(1)
    const [errorMessage, setErrorMessage] = React.useState('')

    //data for divider component
    const border = "solid";
    const color = "current";
    const title = "o";

    //data for Login and register
    let titlemodal:string = "";
    let btngoogle:string = "";
    let btnfacebook:string = "";

    if(isregisterT){
        titlemodal = "Únete a Twitter hoy";
        btngoogle = "Registrate con Google";
        btnfacebook = "Registrate con Facebook";
    }

    if(!isregisterT){
        titlemodal = "Inicia sesión en Twitter";
        btngoogle = "Inicia sesión con Google";
        btnfacebook = "Inicia sesión con Facebook";
    }

    const handleForm = async () => {
        const res = await loginWithEmail(email, password);

        console.log(res)
    }

    const loginGoogle = async(event:any)=>{

         await signInWithGoogle();
        
    }

    const verifyUser = async() => {
        const response = await getUser(email);
        console.log(response)

        if(!response)
        {
        setErrorMessage('Usuario no encontrado')
        return;
        }

        setStep(2);
    }

    const closeModal = () => {
        setStep(1);
        onClose();

    }

    const changeTypeForm = (typeForm:string) => {
        if(typeForm === "register")
        {

            setIsregisterT(true);
        }

        if(typeForm === 'login')
        {
            setIsregisterT(false);

        }
    }

    if( !openModal ) return null
    return (

        <>
            <main className="text-white fixed inset-0 bg-white bg-opacity-25 backdrop-blur-sm flex flex-col justify-center items-center">
                <div className="overflow-auto scr bg-black w-[600px] h-[500px] border rounded-[1.5rem] shadow-md flex flex-col justify-center items-center">

                    <section className="flex justify-between bg-black items-start w-full pt-2 sticky top-0 z-50">
                        <button className="text-white text-xl ml-4" onClick={()=>closeModal()}>X</button>
			            <FaTwitter size={30} color="white" />
                        <div></div>

                    </section>

                    {step === 1 ? (
                         <section className="mx-[6.5rem] mt-[7rem]">
                            <h3 className="text-center font-semibold text-[30px] mb-5">
                                {titlemodal}
                            </h3>

                            <div className="flex flex-col gap-5">
                                <button onClick={loginGoogle}
                                    className="block bg-white text-black rounded-full text-center p-2 font-semibold ">
                                        {btngoogle}
                                </button>
                                <button 
                                    className="block bg-white text-black rounded-full text-center p-2 font-semibold opacity-50 cursor-not-allowed ">
                                    {btnfacebook}
                                </button>
                            </div>

                            <Divider border={border} color={color} title={title}/>

                            {!isregisterT ? (
                                <>
                                    <div className="flex flex-col gap-3">
                                    <input 
                                        onChange={(e) => setEmail(e.target.value)} required
                                        type="email" placeholder="Correo, usuario o telefono" name="email" id="email" 
                                        className="bg-transparent border-2 rounded-md appearance-none py-3 mt-3 focus:border-blue-700 focus:outline-none focus:shadow-outline "

                                    />
                                    </div>

                                    <div className="flex flex-col gap-5 my-5">
                                        <button onClick={verifyUser}
                                            className={email !== '' ? 'block bg-white text-black rounded-full text-center p-2 font-semibold' : 'block bg-white text-black rounded-full text-center p-2 font-semibold opacity-50 cursor-not-allowed'}>
                                                Siguiente
                                        </button>
                                        <button className="block bg-black text-white rounded-full text-center p-2 font-semibold border-2 border-current">¿Olvidaste tu constraseña?</button>
                                        {/* <button onClick={()=> {logOut()}} className="block bg-black text-white rounded-full text-center p-2 font-semibold border-2 border-current">Cerrar sesión</button> */}
                                    </div>

                                    <div className="my-[4rem]">
                                        <span className="text-[18px]"> 
                                            <small className="text-gray-400">¿No tienes una cuenta?</small> 
                                            <button onClick={()=> {changeTypeForm('register')}} className="decoration-none text-blue-800" >Registrate</button> 
                                        </span>
                                    </div>
                                </>
                            ) :
                            (
                                <>
                                <div className="flex flex-col gap-5 my-5">
                                    <button onClick={verifyUser}
                                        className={email !== '' ? 'block bg-white text-black rounded-full text-center p-2 font-semibold' : 'block bg-white text-black rounded-full text-center p-2 font-semibold opacity-50 cursor-not-allowed'}>
                                            Crear cuenta
                                    </button>
                                    {/* <button className="block bg-black text-white rounded-full text-center p-2 font-semibold border-2 border-current">¿Olvidaste tu constraseña?</button> */}
                                    {/* <button onClick={()=> {logOut()}} className="block bg-black text-white rounded-full text-center p-2 font-semibold border-2 border-current">Cerrar sesión</button> */}
                                </div>

                                <div className="my-[4rem]">
                                    <span className="text-[18px]"> 
                                        <small className="text-gray-400">¿Ya tienes una cuenta?</small> 
                                        <button onClick={()=> {changeTypeForm('login')}} className="decoration-none text-blue-800" >Inicia sesión</button> 
                                    </span>
                                </div>

                                </>
                            )


                            }

                        </section>
                    ) : (
                         <section className="mx-[0.2rem] mt-[.5rem]">
                            <h3 className="text-center font-semibold text-[30px] mb-5">
                                Introduce tu contraseña
                            </h3>

                            <div className="flex flex-col justify-around">
                                <div className="flex flex-col gap-3">
                                    <input 
                                        onChange={(e) => setEmail(e.target.value)} required value={email}
                                        type="email" placeholder="Correo, usuario o telefono" name="email" id="email" disabled
                                        className="bg-gray-500 opacity-1 text-white border-2 rounded-md appearance-none py-3 mt-3 focus:border-blue-700 focus:outline-none focus:shadow-outline "
                                    />

                                    <input 
                                        onChange={(e) => setPassword(e.target.value)} required
                                        type="password" placeholder="********" name="password" id="password" 
                                        className="bg-transparent border-2 rounded-md appearance-none py-3 mt-3 focus:border-blue-700 focus:outline-none focus:shadow-outline"

                                    />
                                </div>

                                <div className="flex flex-col gap-5 my-5">
                                    <button onClick={()=>{handleForm()}}
                                        className={password !== '' ? 'block bg-white text-black rounded-full text-center p-2 font-semibold' : 'block bg-white text-black rounded-full text-center p-2 font-semibold opacity-50 cursor-not-allowed'}>
                                            Iniciar Sesión
                                    </button>
                                  
                                    <div className="my-[2rem]">
                                        <span className="text-[18px]"> 
                                            <small className="text-gray-400">¿No tienes una cuenta?</small> 
                                            <a className="decoration-none text-blue-800" href="">Registrate</a> 
                                        </span>
                                    </div>
                                </div>
                            </div>

                           

                        </section>
                    )}

                   
                </div>
            </main>
        </>
    )

}



export default Modal