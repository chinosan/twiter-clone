import React from "react";

interface DividerProp {
    border:string;
    color:string;
    title:string
}

const Divider = ({border = "solid", color = "#8EA3AF", title="Ó" }:DividerProp) => {

    // console.log(title)
    return (

        <>
        <section className="flex justify-center items-center my-3">

            <div className={`  w-full border-${border} border-b border-${color}`}></div>
                <h4 className="px-2">{title}</h4>
            <div className={`  w-full border-${border} border-b border-${color}`}></div>
        </section>
        {/* <section className="flex">

            <div className={`border-2 border-${color}`}></div>
            <div>{title}</div>
            <div className=""></div>
        </section> */}
        </>
    )
    
    
}

export default Divider;