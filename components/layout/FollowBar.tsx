import React from 'react'
import Modal from '../auth/modal-login';
import { useSelector } from 'react-redux';

// interface Preload{
//     hasUser: boolean;
// }

// const FollowBar = ({hasUser}:Preload) => {
const FollowBar = () => {

  const [showModal, setShowModal] = React.useState(false)
  const [isregisterT, setIsregisterT] = React.useState(true)
  const userData = useSelector(state => state.user);

   const openModal = () => {
      setIsregisterT(false);
      setShowModal(true)
    }

  return (
    <div className='px-6 py-4 hidden lg:block ' >
      {userData.hasUser ? 
        <div className="bg-neutral-600  rounded-xl p-4">
            <h2 className='text-white text-xl font-semibold' >WHO TO FOLLOW</h2>
            <div className="flex flex-col gap-6 mt-4 ">
                {/* tod user list */}
            </div>
        </div>
        :
        <>
          <div className="bg-blue-600  rounded-xl p-4 hover:cursor-pointer" onClick={openModal}>
            <h2 className='text-white text-center text-xl font-semibold' >ÚNETE A TWITTER</h2>
            <div className="flex flex-col gap-6 mt-4 ">
                {/* tod user list */}
            </div>
        </div>
        </>
      
      }
      <Modal openModal={showModal} isregisterT={isregisterT} setIsregisterT={setIsregisterT} onClose={()=>{setShowModal(false)}} />

    </div>
  )
}

export default FollowBar