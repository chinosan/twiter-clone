import { BiLogOut } from 'react-icons/bi'
import SidebarItem from './SidebarItem'
import SidebarLogo from './SidebarLogo'
import SidebarTweetButton from './SidebarTweetButton'
import { logOut } from '../../firebase/auth'
import React from 'react'
import { FaAtlas, FaCog, FaUser } from 'react-icons/fa'
import { useSelector } from 'react-redux'
import { useAuthContext } from '../../firebase/AuthContext'
import { IconBaseProps } from 'react-icons'
// interface Preload{
//     userData: any;
//     items:any
// }


// const Sidebar = ({userData, items}:Preload) => {
const Sidebar = () => {

    const {user} = useAuthContext();

    const userData = useSelector(state => state.user);
    let items = [
        {
            label: "Explorar",
            href: "/explorer",
            icon: FaAtlas
        },
        {
            label: "Configurar",
            href: "/configure",
            icon: FaCog
        }
    ];

    React.useEffect(() => {
            // console.log("userData SIDEBAR")
            // console.log(userData.hasUser);


            if(userData.hasUser)
                items = [
                    {
                        label: "Home",
                        href: "/",
                        icon: BsHouseFill
                    },
                    {
                        label: "Notifications",
                        href: "/notifications",
                        icon: BsBellFill
                    },
                    {
                        label: "Profile",
                        href: "/user/123",
                        icon: FaUser
                    }
                ];
            else
                items = [
                    {
                        label: "Explorar",
                        href: "/explorer",
                        icon: BsHouseFill
                    },
                    {
                        label: "Configurar",
                        href: "/configure",
                        icon: BsBellFill
                    }
                ];

                // console.log(items)
            


        },user)
        

  return (
    <div className='col-span-1 h-full pr-4 md:pr-6' >
        <div className="flex flex-col items-end" >
            <div className="space-y-2 lg:w-[230px] ">
                <SidebarLogo />
                {
                    items.map(item => (
                        <SidebarItem 
                            key={item.href}
                            href={item.href}
                            label={item.label}
                            icon={item.icon}
                        />
                    ))

                }

                {
                    userData?.hasUser ? 
                    <>
                        <div onClick={()=> {logOut()} }>

                            <SidebarItem 
                                label="Logout"
                                icon={BiLogOut}
                                href="/logout"
                            />
                        </div>

                        <SidebarTweetButton />
                    </>
                    :
                    <></>
                }
            </div>
        </div>
    </div>
  )
}

export default Sidebar

function BsHouseFill(props: IconBaseProps): Element {
    throw new Error('Function not implemented.')
}
function BsBellFill(props: IconBaseProps): Element {
    throw new Error('Function not implemented.')
}

