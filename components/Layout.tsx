import FollowBar from "./layout/FollowBar";
import Sidebar from "./layout/Sidebar";
import { useRouter } from 'next/router'
import { AuthContextProvider } from '../firebase/AuthContext'
import React from "react";
interface LayoutProps {
    children: React.ReactNode;
}
const Layout: React.FC<LayoutProps> = ({children}) => {

  const router = useRouter();

  const routerswithlayout = ['/','/profile', '/settings'];

  
  const result = routerswithlayout.includes(router.pathname);
//   console.log(result);

  if(result)
    return (
      <div className="h-screen bg-black" >
          <div className="container h-full mx-auto xl:px-30 max-w-6xl ">
              <div className="grid grid-cols-4 h-full">
                  <Sidebar />
                  <div className="col-span-3  lg:col-span-2 border-x-[1px] border-neutral-800">
                    <AuthContextProvider>
                        {children}
                    </AuthContextProvider>
                  </div>
                  <FollowBar />
              </div>
          </div>
      </div>
    )
  else
      return (
      <div className="h-screen bg-black" >
          <div className="h-full  ">
              <div className="h-full">
                  <div className="border-x-[1px] border-neutral-800">
                  {children}
                  </div>
              </div>
          </div>
      </div>
    )
      
}

export default Layout