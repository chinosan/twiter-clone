import type { Timestamp } from "@firebase/firestore-types";
export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL?: string;
  createdAt: Timestamp;
  userName?: string;
  following?: Array<string>;
  two_factor_enabled?: boolean;
  two_factor_secret?: string;
}
