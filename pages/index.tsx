import { NextPage } from 'next'
import React from 'react'

import {useAuthContext} from '../firebase/AuthContext'

import {userAction} from '../features/users/usersSlice'
import { useDispatch, useSelector } from 'react-redux'


const Home : NextPage = () => {
    const {user} = useAuthContext();

    const dispatcher = useDispatch();
    let userData = useSelector(state => state.user)


    React.useEffect(() => {
      // console.log(user)
        if(user)
        {
          dispatcher(userAction.setCurrentUserlogged(user));
        }
        else
        {
          dispatcher(userAction.setCurrentUserlogged(null));
        }

    }, [user])

    return(
        <section className='h-[100vh] flex justify-center items-center'>
        { userData.hasUser ?
            <>
                <h2 className='text-white text-center font-bold text-[32px]'>Si ves esto estas logeando papu... guiño guiño &#128147;</h2>     
            </>
            :
            <>
                <h2 className='text-white text-center font-bold text-[32px]'>No estas logeado papu  &#128148;</h2>
            </>
        }
        </section>
    )
}

export default Home;
