import { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'

import React, { useState } from "react";
import { logOut} from "../firebase/auth";
// import {addData} from "../firebase/auth";
// import { useRouter } from 'next/navigation'
import Modal from '../components/auth/modal-login';



const Login: NextPage = () => {
    
    const [showModal, setShowModal] = React.useState(false)
    const [isregisterT, setIsregisterT] = React.useState(true)
    // const router = useRouter()

    const handle = (type:string) => {

        switch (type) {
            case "register":
                setIsregisterT(true);
                setShowModal(true)
                break;
            case "login":
                setIsregisterT(false);
                setShowModal(true)
                break;

            case "logout":
                logOut()
                
                break;
        
            default:
                break;
        }
    }





    return(
        <>
            {/* <Modal></Modal> */}
            <section className="bg-gray-200 flex justify-center h-[100vh]">

                <div className='xl:w-[32%] lg:w-[32%] md:w-[50%] sm:w-[90%] xs:w-[90%]'>
                    
                        <div className="flex items-center justify-between mt-4">
                            {/* <button onClick={googleLog} type="button" className="bg-sky-500 hover:bg-sky-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" > */}
                            <button onClick={() => {handle('login')}} type="button" className="bg-sky-500 hover:bg-sky-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" >
                                Login
                            </button>
                             <button onClick={() => {handle('register')}} type="button" className="bg-sky-500 hover:bg-sky-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" >
                                Register
                            </button>
                            {/* <button onClick={()=>{handle('logout')}} className="bg-sky-500 hover:bg-sky-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" >
                                Logout
                            </button> */}
                        </div>

                </div>

            </section>
            <Modal openModal={showModal} isregisterT={isregisterT} setIsregisterT={setIsregisterT} onClose={()=>{setShowModal(false)}} />


        </>
    )
}

export default Login;
