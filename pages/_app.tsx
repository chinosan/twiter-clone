import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Layout from '../components/Layout'
import React from 'react';
// import { useAuthContext } from '../firebase/AuthContext';
import { Provider } from 'react-redux';
import store from '../store';
// import { useDispatch, useSelector } from 'react-redux/es/exports';
// import {setCurrentUserlogged} from '../features/users/usersSlice'


function MyApp({ Component, pageProps }: AppProps) {

  // const {user}  = useAuthContext();

  //   const [userData, setUserData] = React.useState(null)

    // const dispatcher = useDispatch();
    // let userData = useSelector(state => state.user)

    // function setCurrentUser(user:any){
    //   dispatcher(setCurrentUserlogged(user))
    // }


    // React.useEffect(() => {
    //     console.log("user")
    //     console.log(user)
    //     if(user)
    //     {
    //         setUserData(user)
    //         // setCurrentUser(user)
    //     }
    //     else
    //     {
    //       // setCurrentUser(null)
    //         setUserData(null)
    //     }
    //     console.log(user)

    // }, [user])


  return (
  <>
    <Provider store={store}>
      <Layout >
        <Component {...pageProps} />
      </Layout>
    </Provider>

  </>

  )
}

export default MyApp
